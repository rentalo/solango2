#
# Copyright 2008 Optaros, Inc.
#

from solango.solr.documents import SearchDocument
from solango.solr.indexes.base import Index
from solango.solr.query import Query
from solango.registry import documents

SearchDocument # Pyflakes
Query # Pyflakes
documents # Pyflakes
Index # Pyflakes


from solango.solr.fields import *
from solango.exceptions import *



#### Taken from django.contrib.admin.__init__.py
def autodiscover():
    """
    Auto-discover INSTALLED_APPS search.py modules and fail silently when 
    not present. This forces an import on them to register any search bits they
    may want.
    """
    import imp
    from django.conf import settings

    for app in settings.INSTALLED_APPS:
        # For each app, we need to look for an admin.py inside that app's
        # package. We can't use os.path here -- recall that modules may be
        # imported different ways (think zip files) -- so we need to get
        # the app's __path__ and look for admin.py on that path.

        # Step 1: find out the app's __path__ Import errors here will (and
        # should) bubble up, but a missing __path__ (which is legal, but weird)
        # fails silently -- apps that do weird things with __path__ might
        # need to roll their own admin registration.
        try:
            app_path = __import__(app, {}, {}, [app.split('.')[-1]]).__path__
        except AttributeError:
            continue

        # Step 2: use imp.find_module to find the app's admin.py. For some
        # reason imp.find_module raises ImportError if the app can't be found
        # but doesn't actually try to import the module. So skip this app if
        # its admin.py doesn't exist
        try:
            imp.find_module('search', app_path)
        except ImportError:
            continue

        # Step 3: import the app's admin file. If this has errors we want them
        # to bubble up.
        __import__("%s.search" % app)

#if not documents:
#    autodiscover()


class Core(object):
    def __init__(self, connection, data_dir, schema_path):
        self.connection = connection
        self.data_dir = data_dir
        self.schema_path = schema_path


class CoreRegistry(object):
    def __init__(self):
        self._connections = {}

    def configure(self, name, select_url, update_url, ping_url,
            data_dir, schema_path):
        from solango.solr.connection import SearchWrapper

        if name in self._connections:
            raise ValueError('Connection %s already configured' % name)

        self._connections[name] = Core(
            connection=SearchWrapper(update_url, select_url, [ping_url]),
            data_dir=data_dir, schema_path=schema_path)

    def remove(self, name):
        del self._connections[name]

    def __getitem__(self, idx):
        if not idx in self._connections:
            from solango import conf
            config = conf.SOLR_CORES[idx]
            self.configure(idx, config['SELECT_URL'],
                config['UPDATE_URL'], config['PING_URL'],
                config.get('DATA_DIR'), config.get('SCHEMA_PATH'))
        return self._connections[idx]


cores = CoreRegistry()

