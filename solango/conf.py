#
# Copyright 2008 Optaros, Inc.
#

from django.conf import settings

### Search-specific settings.

SOLR_CORES = getattr(settings, 'SOLR_CORES', {
    'default': {
        'UPDATE_URL': 'http://localhost:8983/solr/update',
        'SELECT_URL': 'http://localhost:8983/solr/select',
        'PING_URL': 'http://localhost:8983/solr/admin/ping',
        'SCHEMA_PATH': None,
        'DATA_DIR': None,
        },
    })

SOLR_HOME = getattr(settings, 'SOLR_HOME', None)
SOLR_ROOT = getattr(settings, 'SOLR_ROOT', None)
SOLR_DEFAULT_CORE = getattr(settings, 'SOLR_DEFAULT_CORE', 'default')


### BATCH INDEX SIZE
SOLR_BATCH_INDEX_SIZE = getattr(settings,"SOLR_BATCH_INDEX_SIZE", 10)

#### Default Query Operator
SOLR_DEFAULT_OPERATOR = getattr(settings, "SOLR_DEFAULT_OPERATOR", "OR")

### Default Sorting criteria
SEARCH_SORT_PARAMS = getattr(settings, "SEARCH_SORT_PARAMS", {
        # "field direction": "anchor" The anchor for display purposes
        "score desc": "Relevance" 
})

### Default Facet Settings
SEARCH_FACET_PARAMS =  getattr(settings, "SEARCH_FACET_PARAMS", [
    ("facet", "true"),             # Basic faceting
    ("facet.field", "model"),      # Facet by model
    
    # date faceting
    #("facet.date", "date"),      # Replace date with the name of your solr date field
    #("facet.date.start", "2000-01-01T00:00:00.000Z"),
    #("facet.date.end", "NOW/DAY+1DAY"),
    #("facet.date.gap", "+1YEAR"),
])

### strftime format strings for displaying faceting links.
SEARCH_FACET_DATE_FORMATS  = getattr(settings, "SEARCH_FACET_DATE_FORMATS", {
    'DAY': '%e %b %Y',
    'MONTH': '%B %Y',
    'YEAR': '%Y',
})

SEARCH_HL_PARAMS = getattr(settings, "SEARCH_HL_PARAMS" ,[
    ("hl", "true"),      # basic highlighting
    ("hl.fl", "text"),   # What field to highlight
])

# Lucene Special Characters
# + - && || ! ( ) { } [ ] ^ " ~ * ? : \
SEARCH_SEPARATOR = getattr(settings, "SEARCH_SEPARATOR", "__")

FACET_SEPARATOR = getattr(settings, "FACET_SEPARATOR", ";;")

########## DEFERRED_BACKEND ##########
DEFERRED_BACKEND = "database"

