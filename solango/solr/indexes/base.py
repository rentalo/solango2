
from solango.deferred import defer
from solango.solr.connection import SearchWrapper
from solango.solr.query import Query

from solango import conf


class Index(object):
    """
    Solr Index
    """

    def __init__(self, name=None, connection=None):

        self.name = name or 'default_index'
        self._connection = connection

    def get_document(self, instance):
        from solango import documents
        return documents.get_document(instance)

    @property
    def connection(self):
        """Lazy Init a Collection """
        if not self._connection:
            from solango import cores
            self._connection = cores[conf.SOLR_DEFAULT_CORE].connection
        return self._connection

    def query(self, initial=None, **kwargs):
        """
        Creates a default query.
        """
        default_initial = conf.SEARCH_FACET_PARAMS
        default_initial.extend(conf.SEARCH_HL_PARAMS)
        default = Query(default_initial , sort=conf.SEARCH_SORT_PARAMS.keys())

        query = Query(initial, **kwargs)

        default.merge(query)

        return default

    def ping(self):
        return self.connection.is_available()

    def optimize(self):
        return self.connection.optimize()

    def commit(self):
        return self.connection.commit()

    def add(self, doc, commit=True):
        return self.connection.add(doc.to_add_xml(), commit)

    def delete(self, doc, commit=True):
        return self.connection.delete(doc.to_delete_xml(), commit)

    def delete_all(self, commit=True):
        return self.connection.delete_all(commit)

    def delete_by_query(self, query, commit=True):
        return self.connection.delete_by_query(query, commit)

    def select(self, initial=None, **kwargs):
        if isinstance(initial, Query):
            query= initial
        else:
            query = self.query(initial, **kwargs)
        return self.connection.select(query)

    def reindex(self, model, doc, batch_size=50, queryset=None, commit=True):
        start = 0
        xml = ""
        base_qs = (queryset or model._default_manager.all()).order_by('pk')

        while True:
            qs = base_qs[start:start+batch_size]
            if not qs:
                break
            for i in qs:
                d = doc(i)
                if d.is_indexable(i):
                    xml += d.to_add_xml()
                else:
                    #Add will commit.
                    self.delete(d, False)

            if xml:
                results = self.connection.add(xml, commit)

                for result in results:
                    if not result.success:
                        self.defer("add", result.xml, error=result.error)

            xml = ""
            start += batch_size

    def reindex_qs(self, queryset, batch_size=50, commit=True):
        doc = self.get_document(queryset[0]).__class__
        model = queryset.model
        return self.reindex(model, doc, batch_size=batch_size,
                queryset=queryset, commit=commit)

    def post_save(self, sender, instance, **kwargs):
        doc = self.get_document(instance)
        self.add(doc)

    def post_delete(self, sender, instance, **kwargs):
        doc = self.get_document(instance)
        self.delete(doc)

    def defer(self, method, xml, doc_pk=None, error=None):
        defer.add(method, xml, doc_pk, error)
