#
# Copyright 2008 Optaros, Inc.
#

from datetime import datetime, timedelta
import urllib2
import copy

from solango import conf
from solango.log import logger
from solango.solr import results
from solango.solr.query import Query
from solango.exceptions import SolrUnavailable, SolrException

(DELETE, ADD) = (0,1)


class LazyQuery(object):
    def __init__(self,  wrapper, query):
        self._wrapper = wrapper
        self._query = query
        self._result = None

    def _clone(self):
        """
        Makes clone of LazyQuery
        """
        return LazyQuery(self._wrapper, copy.deepcopy(self._query))

    def _load(self):
        """
        Fetch data from SOLR using SearchWrapper instance
        """
        request_url = self._wrapper.select_url + self._query.url()
        return self._wrapper._select_request(request_url)

    def _get_result(self):
        """
        Returns results object. Documents will be automatically loaded
        if cache is empty.
        """
        if self._result is None:
            self._result = self._load()
        return self._result

    def __len__(self):
        """
        Returns number of documents
        """
        return self._get_result().count

    def __nonzero__(self):
        """
        Returns True if result contains at least one document
        """
        return self._get_result().count>0

    def __iter__(self):
        """
        Creates iterator over documents from result.
        """
        return iter(self._get_result().documents)

    def __getitem__(self, k):
        """
        Retrieves an item or slice from the set of results.

        Code is taken from django.db.models.query.QuerySet.__getitem__ 
        with little modifications.
        """
        if not isinstance(k, (slice, int, long)):
            raise TypeError

        # negative indexing is not supported
        assert ((not isinstance(k, slice) and (k >= 0))
                or (isinstance(k, slice) and (k.start is None or k.start >= 0)
                    and (k.stop is None or k.stop >= 0))), \
                "Negative indexing is not supported."

        if isinstance(k, slice):
            if k.start is not None:
                start = int(k.start)
            else:
                start = None
            if k.stop is not None:
                stop = int(k.stop)
            else:
                stop = None
            start, stop = start or 0, stop or 0

            # check whether a subset of documents is already loaded in result cache
            if self._result:
                r_start = self._result.start
                r_stop = self._result.rows + r_start

                if start>=r_start and stop<=r_stop:
                    # cache hit
                    return self._result.documents[start-r_start:stop-r_start]

            # otherwise load data into new query instance
            # and return subset of documents
            query = self._clone()
            query._query.start.set(start)
            query._query.rows.set(stop-start)
            return k.step and list(query)[::k.step] or query

        # check whether document already exists in results cache
        if self._result:
            r_start = self._result.start
            r_stop = self._result.rows + r_start

            if k>=r_start and k<=r_stop:
                # cache hit
                return self._result.documents[k-r_start]

        # otherwise load data into new query instance
        # and return specified document
        query = self._clone()
        query._query.start.set(k)
        query._query.rows.set(1)
        return list(query)[0]

    def __getattr__(self, attr):
        return getattr(self._get_result(), attr)


class SearchWrapper(object):
    """
    This class is the entry point for all search-bound actions, including
    adding (indexing), deleting, and selecting (searching).
    """
    
    available = False
    heartbeat = None
    
    def __init__(self, update_url, select_url, ping_urls):
        """
        Resolves configuration and instantiates a Log for this object.
        """
        self.update_url = update_url
        self.select_url = select_url
        self.ping_urls = ping_urls
        self.heartbeat = datetime(1970, 01, 01)
    
    def is_available(self):
        """
        Returns True if the search system appears to be available and in good
        health, False otherwise.  A ping is periodically sent to the search
        server to query its availability.
        """
        (now, delta) = (datetime.now(), timedelta(0, 300))
        
        if now - self.heartbeat > delta:
            try:
                for url in self.ping_urls:
                    res = urllib2.urlopen(url).read()
            except StandardError:
                self.available = False
            else:
                self.available = True
                self.heartbeat = now
            
        return self.available
    
    
    
    def _update_request(self, method, xml):
        """
        Issues update requests
        """
        
        if not xml:
            raise SolrException("No XML to Add")
        
        xml = xml.encode("utf-8", "replace")
        
        request = urllib2.Request(self.update_url, xml)
        request.add_header("Content-type", "text/xml; charset=utf-8")
        
        response = None
        try:
            response = urllib2.urlopen(request)
        except urllib2.HTTPError, e:
            return results.ErrorResults(method, self.update_url, xml, str(e), e.code)
        except urllib2.URLError, e: 
            return results.ErrorResults(method, self.update_url, xml, str(e))
        
        return results.UpdateResults(response.read())
    
    def _select_request(self, url):
        """
        Issues update requests
        """
        
        request = urllib2.Request(url)
        request.add_header("Content-type", "application/json; charset=utf-8")

        response = None
        try:
            response = urllib2.urlopen(request)
        except urllib2.HTTPError, e:
            return results.SelectErrorResults(url, str(e), e.code)
        except  urllib2.URLError, e:
            return results.SelectErrorResults(url, str(e))
        
        return results.SelectResults(url, response.read())

       
    def add(self, xml, commit=True):
        """
        Adds the specified list of objects to the search index.  Returns a
        two-element List of UpdateResults; the first element corresponds to
        the add operation, the second to the subsequent commit operation.
        """
        
        if xml:
            xml = "\n<add>\n" + xml + "</add>\n"
        
        results=[]
        
        results.append(self._update_request("add", xml))
        
        if commit:
            results.append(self.commit())
        
        return results
    
    def delete_all(self, commit=True):
        return self.delete_by_query(q='*:*', commit=commit)

    def delete_by_query(self, q, commit=True):

        results=[]
        
        results.append(self.update(
            unicode("\n<delete><query>%s</query></delete>\n" % q, "utf-8")
        ))
        if commit:
            results.append(self.commit())

        return results

    def delete(self, xml, commit=True):
        """
        Deletes the specified list of objects from the search index.  Returns
        a two-element List of UpdateResults; the first element corresponds to
        the delete operation, the second to the subsequent commit operation.
        """
        
        if xml:
            xml = "\n<delete>\n" + xml + "</delete>\n"
                
        results=[]
        
        results.append(self._update_request("delete", xml))
        
        if commit:
            results.append(self.commit())
        
        return results
    
    def commit(self):
        """
        Commits any pending changes to the search index.  Returns an
        UpdateResults instance.
        """
        return self._update_request("commit", 
                                    unicode("\n<commit/>\n", "utf-8"))
    
    def optimize(self):
        """
        Optimizes the search index.  Returns an UpdateResults instance.
        """
        return self._update_request("optimize", 
                                    unicode("\n<optimize/>\n", "utf-8"))
    
    def update(self, xml):
        """
        Submits the specified Unicode content to Solr's update interface (POST).
        """
        return self._update_request("update", xml)
    
    def select(self, initial=None, **kwargs):
        """
        Submits the specified query to Solr's select interface (GET).
        """
        
        if initial and isinstance(initial, Query):
            query= initial
        else:
            query = Query(initial, **kwargs)

        return LazyQuery(self, query)
